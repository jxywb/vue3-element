import mitt from "mitt"
const model = {
  select: "",
  input: ""
}
const rules = {
  select: {
    reuqired: false,
    message: "请选择下拉框"
  },
  input: {
    reuqired: true,
    message: "文本应该为必填"
  }
}
const bus = mitt()
const handleVaild = (item, key) => {
  if (item.reuqired && !model[key] && model[key].length == 0) {
    console.log(item.message)
  }
}
const setVaild = (rules) => {
  Object.keys(rules).forEach((key) => {
    bus.on(key, () => handleVaild(rules[key], key))
  })
}
const triggerVaild = () => {
  Object.keys(rules).forEach((key) => {
    bus.emit(key)
  })
}
setVaild(rules)
// 使用策略模式
triggerVaild()
