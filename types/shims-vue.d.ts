declare module "*.vue" {
  import { DefineComponent } from "vue"
  const component: DefineComponent<{}, {}, any>
  export default component
}

declare module "mockjs" {
  const Mock: any
  export default Mock
}

declare const __APP_VERSION__: string
declare const __APP_MESSAGE__: string
declare const __APP_DATE__: string
