import Mock from "mockjs"

Mock.mock("/api/user", "get", {
  status: 200,
  "data|1-10": [
    {
      "name|1": ["Amy", "John", "Kim", "Lily", "Lucy", "Tom"],
      "id|+1": 1,
      "age|18-28": 20
    }
  ]
})

Mock.mock(RegExp("/api/login" + ".*"), "get", (options) => {
  const params = JSON.parse(options.body)
  if (params.username === "Yoge") {
    return {
      status: 200,
      data: {
        name: "Yoge",
        id: 1,
        "age|18-28": 20,
        token: "admin_token",
        menus: [
          {
            title: "首页",
            path: "/home",
            child: []
          },
          {
            title: "组件测试",
            path: "/oms",
            child: [
              {
                title: "用户管理",
                path: "/oms/index",
                child: []
              },
              {
                title: "角色管理",
                path: "/system/role",
                child: []
              }
            ]
          }
        ]
      }
    }
  } else if (params.username === "user") {
    return {
      status: 200,
      data: {
        name: "User",
        id: 2,
        "age|18-28": 20,
        token: "user_token",
        menus: [
          {
            title: "首页",
            path: "/home",
            child: []
          },
          {
            title: "组件测试",
            path: "/oms",
            child: [
              {
                title: "用户管理",
                path: "/oms/index",
                child: []
              },
              {
                title: "角色管理",
                path: "/system/role",
                child: []
              }
            ]
          }
        ]
      }
    }
  } else {
    return {
      status: 400,
      message: "Unknown user"
    }
  }
})
