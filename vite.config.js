import { defineConfig } from "vite"
import UnoCSS from "unocss/vite"
import { presetIcons } from "unocss"
import presetWind from "@unocss/preset-wind"
import vue from "@vitejs/plugin-vue"
import AutoImport from "unplugin-auto-import/vite"
import Components from "unplugin-vue-components/vite"
import { ElementPlusResolver } from "unplugin-vue-components/resolvers"
import { resolve } from "path"
import git from "simple-git"
import * as fs from "fs"
let SysVersion = null
let SysMessage = null
let SysMdate = null
const getGitVersion = async () => {
  const dayjs = require("dayjs")
  const date = dayjs().format("YYYY-MM-DD HH:mm:ss")
  const data = await git().log(["-1"])
  const { hash, message } = data.latest
  SysVersion = hash
  SysMessage = message
  SysMdate = date
}

// https://vitejs.dev/config/
export default defineConfig(async () => {
  await getGitVersion()

  return {
    server: {
      /** 是否开启 HTTPS */
      https: {
        key: fs.readFileSync(`${__dirname}/mkcert/localhost-key.pem`),
        cert: fs.readFileSync(`${__dirname}/mkcert/localhost.pem`)
      },
      // https:false,
      /** 设置 host: true 才可以使用 Network 的形式，以 IP 访问项目 */
      host: true, // host: "0.0.0.0"
      /** 端口号 */
      port: 3001,
      /** 是否自动打开浏览器 */
      open: false,
      /** 跨域设置允许 */
      cors: true,
      /** 端口被占用时，是否直接退出 */
      strictPort: false,
      /** 接口代理 */
      proxy: {
        "/api": {
          target: "https://www.yoge.fun/api/", // 目标服务器地址
          changeOrigin: true, // 是否改变请求头中的Origin字段
          rewrite: (path) => path.replace(/^\/api/, "") // 将请求地址中的/api转换为实际的api地址
        }
      }
    },
    define: {
      __APP_VERSION__: JSON.stringify(SysVersion),
      __APP_MESSAGE__: JSON.stringify(SysMessage),
      __APP_DATE__: JSON.stringify(SysMdate)
    },
    resolve: {
      alias: {
        /** @ 符号指向 src 目录 */
        "@": resolve(__dirname, "./src")
      }
    },
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: `@use "@/assets/common.scss" as *;`
        }
      }
    },
    plugins: [
      AutoImport({
        imports: ["vue", "vue-router", "vuex", "pinia", "@vueuse/core"],
        dirs: ["./src/views/**/*.vue"],
        resolvers: [ElementPlusResolver()],
        eslintrc: {
          enabled: false, // Default `false`
          filepath: "./.eslintrc-auto-import.json", // Default `./.eslintrc-auto-import.json`
          globalsPropValue: true // Default `true`, (true | false | 'readonly' | 'readable' | 'writable' | 'writeable')
        }
      }),
      Components({
        dirs: ["./src/components"],
        resolvers: [ElementPlusResolver({ importStyle: "sass" })]
      }),
      vue(),
      UnoCSS({
        presets: [
          presetWind(),
          presetIcons({
            collections: {
              logos: () => import("@iconify-json/logos/icons.json").then((i) => i.default),
              carbon: () => import("@iconify-json/carbon/icons.json").then((i) => i.default)
            }
          })
        ]
      })
    ]
  }
})
