import { request } from "@/plugins/http"
import { TokenResponseData } from "./types/index"
/** 登录 */
export function login(data: any) {
  return request<TokenResponseData>({
    url: "/api/login",
    method: "post",
    data
  })
}

/** 条形码 */
export function getCaptcha() {
  return request({
    url: "/api/captcha",
    method: "get"
  })
}

/** oms */
export function getOms() {
  return request({
    url: "/api/oms",
    method: "get"
  })
}

/** oms */
export function getUser() {
  return request({
    url: "/api/user",
    method: "get"
  })
}
