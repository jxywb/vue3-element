import { useGlobalStore } from "@/store/index"
type ColorKeys = "primary" | "success" | "warning" | "error" | "info"

export type Theme = {
  light: {
    color: Record<ColorKeys, string>
    bg: Record<ColorKeys, string>
  }
  dark: {
    color: Record<ColorKeys, string>
    bg: Record<ColorKeys, string>
  }
}

export const theme: ref<Theme> = useStorage("theme", {
  light: {
    color: {
      primary: "#409eff",
      success: "#67C23A",
      warning: "#E6A23C",
      danger: "#F56C6C",
      info: "#909399"
    },
    bg: {
      primary: "white",
      success: "#67C23A",
      warning: "#E6A23C",
      danger: "#F56C6C",
      info: "#909399"
    },
    text: {
      primary: "black"
    },
    border: {
      color: "#dcdfe6"
    }
  },
  dark: {
    color: {
      primary: "#409eff",
      success: "#67C23A",
      warning: "#E6A23C",
      error: "#F56C6C",
      info: "#909399"
    },
    bg: {
      primary: "black",
      success: "#67C23A",
      warning: "#E6A23C",
      error: "#F56C6C",
      info: "#909399"
    },
    text: {
      primary: "black"
    },
    border: {
      color: "black"
    }
  }
})

// 设置css变量
function setStyleProperty(propName: string, value: string) {
  document.documentElement.style.setProperty(propName, value)
}

//设置主题
export const setTheme = (themeKey: string | null, init: Boolean = false) => {
  const store = useGlobalStore()
  if (!init) {
    store.toggleTheme(themeKey || store.themeName)
  }
  const themeName = store.themeName
  debugger
  if (themeName) {
    const currentTheme = theme.value[themeName]
    Object.keys(currentTheme).forEach((key: any) => {
      Object.keys((currentTheme as any)[key]).forEach((colorKey: ColorKeys) => {
        setStyleProperty(`--el-${key}-${colorKey}`, `${(currentTheme as any)[key][colorKey]}`)

        theme.value[themeName][key][colorKey] = `${(currentTheme as any)[key][colorKey]}`
      })
    })
  }
}

export const setColor = (k: string, c: string | null) => {
  const store = useGlobalStore()
  setStyleProperty(`--el-color-${k}`, c)
  const currentTheme = theme.value[store.themeName].color
  currentTheme[k] = c
}
