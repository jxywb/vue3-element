import { createApp } from "vue"
import { createPinia } from "pinia"
import piniaPluginPersistedstate from "pinia-plugin-persistedstate"
import "uno.css"
import "@unocss/reset/tailwind.css"
import router from "./router/index"
import App from "@/App.vue"
// import { setTheme } from "./views/layout/setTheme"
import "../mock/index.js"
const app = createApp(App)
const pinia = createPinia()
pinia.use(piniaPluginPersistedstate)

app.use(router)
app.use(pinia)

app.mount("#app")
// setTheme(null)
