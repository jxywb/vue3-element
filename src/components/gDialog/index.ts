import { reactive } from "vue"

type dialogOptions = {
  title: string
  component: any
  props?: Object
  width: string
  visible?: any
  show: boolean
  callBack?: Function
  pathKey: String
}
export const dialogList: dialogOptions[] = reactive([])
const cacheDialog: Array<number> = reactive([])
let router: any = null
export const init = (data: any) => {
  router = data
}

export const addDialog = (options: dialogOptions) => {
  dialogList.push(Object.assign(options, { visible: true, pathKey: router.currentRoute.value.path, show: true }))
}

export const closeDialog = (item: dialogOptions, i: number, args?: any, isNativeClose?: boolean) => {
  item.visible = false
  cacheDialog.push(i)
  if (!isNativeClose) item.callBack && item.callBack(...args)
  setTimeout(() => {
    // 加上动效并延时清除缓存
    while (cacheDialog?.length) {
      const index = cacheDialog.shift()
      if (index === undefined) break
      dialogList.splice(index, 1)
    }
  }, 500)
}
