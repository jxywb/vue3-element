export type columnsType = {
  label: string
  value: string
  slot: Boolean
  slotName: string
  [key: string]: any
}
