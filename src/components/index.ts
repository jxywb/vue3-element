import type { Component } from "vue"
import Gtable from "./gtable/index.vue"
const components: {
  [propName: string]: Component //字面量类型，每个属性值类型为组件的类型
} = {
  Gtable
}
export default components
