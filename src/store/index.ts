import { theme } from "@/views/layout/setTheme"
export const useGlobalStore = defineStore("globalStore", {
  persist: true,
  state: () => {
    return {
      isCollapse: false,
      themeName: "light",
      tabList: [
        {
          path: "/index",
          name: "index",
          title: "首页"
        }
      ],
      defaultActive: "/index"
    }
  },
  actions: {
    getBgTheme(themeName?: string | null) {
      const currentTheme = theme.value[themeName || this.themeName].bg
      return currentTheme.primary
    },
    toggele() {
      this.isCollapse = !this.isCollapse
    },
    toggleTheme() {
      this.themeName = this.themeName == "dark" ? "light" : "dark"
      this.setBgColor = useCssVar("--el-bg-primary", document.documentElement, {
        initialValue: this.themeName == "dark" ? "black" : "white"
      })
      // document.documentElement.setAttribute("data-theme", this.themeName)
    },
    setDefaultActive(name: string) {
      this.defaultActive = name
    },
    clearTab() {
      this.tabList = [
        {
          path: "/index",
          name: "index",
          title: "首页"
        }
      ]
      this.defaultActive = "/index"
    },
    closeOtherTab(i: string) {
      this.tabList = this.tabList.filter((item: any, index: any) => index == 0 || i == index)
    },
    addTab(tab: any) {
      if (this.tabList.some((item: any) => item.path === tab.path)) return
      this.tabList.push(tab)
    },
    changeColor(key: string, color: string | null) {
      this[key] = color
    }
  }
})
