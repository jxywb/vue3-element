# 安装

npm run install

```ts
├── src
├── assets // 存放静态资源
├── baseComponents // 基础组件
│ └──Form // 表单组件
│ │ ├──Form.vue
│ │ └──index.ts
│ ├── Input // 输入框组件
│ │ ├──Input.vue
│ │ └──index.ts  
 │ ....
├── components // 业务组件
│ ├── BookMark // 页面主要内容相关组件
│ │ ├──BookMark.vue
│ │ ├──useLabels.ts
│ │ └──index.ts  
 │ .....
├── hooks // 封装的一些复用逻辑
├── styles // 样式
├── utils // 存放自己封装的工具
├── APP.vue
└── main.ts
```
